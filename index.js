function Pokemon (name, level){
	
	// declare the needed properties, using the this keyword
	// Properties
	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = level;

	// declare the object methods here
	this.tackle = function(opponent){
		console.log(`${this.name} tackled ${opponent.name}`)
		let result = opponent.health - this.atk;
		opponent.health = result;
					
		console.log(`${opponent.name}'s health is now reduced to ${result}`)
		
		return result <= 10 ? opponent.faint() : console.log(`${opponent.name}'health is above 10`)
		
	};
		this.faint = function(){
			console.log(`${this.name} fainted`)
		};
	};

let jigglypuff = new Pokemon("jigglypuff", 20);
let snorlax = new Pokemon("snorlax", 2);
console.log(jigglypuff);
console.log(snorlax);


jigglypuff.tackle(snorlax);
snorlax.tackle(jigglypuff);
// jigglypuff.tackle(snorlax);
// snorlax.tackle(jigglypuff);
// jigglypuff.tackle(snorlax);
// snorlax.tackle(jigglypuff);
// snorlax.tackle(jigglypuff);
// jigglypuff.faint();